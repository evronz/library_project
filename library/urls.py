from django.urls import path
from . import views
app_name = 'library'

urlpatterns = [
    path("",views.book_overview,name="book_overview"),
    path("<int:pk>/",views.book_detail,name="book_detail"),
    path("<int:pk>/borrow/", views.borrow, name="borrow"),
    path("activity", views.activity, name="activity"),
    path("urlmessage", views.urlmessage, name="message"),
    ]
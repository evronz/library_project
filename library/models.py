#from datetime import datetime
from django.db import models
from datetime import date, datetime, timedelta

# Create your models here.


class Book(models.Model):
    STATUS = (
        ('Available', 'Available'),
        ('Unavailable', 'Unavailable'),
    )
    title            = models.CharField(max_length=40)
    publication_date = models.DateField(auto_now=True)
    subject_area     = models.CharField(max_length=15)
    author           = models.CharField(max_length=40)
    description      = models.TextField()
    cover_photo      = models.ImageField(null=True, blank=True, upload_to='static/images/')
    status           = models.CharField(max_length=15, choices=STATUS, default='Available')

    def __str__(self) :
        return self.title





class Borrow(models.Model):
    FINE = (
        ("None", "None"),
        ("5000", "5000"),
        ("10000", "10000"),
        ("15000", "15000"),
    )
    book_title    = models.ForeignKey('Book', on_delete=models.CASCADE)
    borrow_date   = models.DateField(auto_now=True)
    email_address = models.EmailField('Email Field')
    fine          = models.CharField(max_length=6, choices=FINE, default='None')

    def __str__(self):
        return self.book_title.title
    # use this in table that will leep track of days

    
    

    @property
    def days_remaining(self):
        today = date.today()
        days_togo= self.return_date - today
        return days_togo.days

    @property
    def return_date(self):
        that_date = self.borrow_date + timedelta(days=3)
        return that_date

    @property
    def Fine(self):
        dafine = self.fine
        if self.days_remaining <= -3:
            dafine = "5000"

        elif self.days_remaining <= -10:
            dafine = "10000"
        
        elif self.days_remaining <= -15:
            dafine = "15000"

        else:
            dafine = "None"

        return dafine
        

        # today = date.today()
        # if (self.return_date < today):
        #     if (today - self.return_date) >= timedelta(days=3)
        #     returned = "PAST"
        # return returned





from django.shortcuts import render, redirect
from .models import Book, Borrow
#from Borrow import Days_remainng
from .models import Book
from .forms import BorrowForm
from django.contrib import messages
from django.urls import reverse_lazy
#from datetime import date,timedelta

# Create your views here.
def book_overview(request):
    books = Book.objects.all().order_by('-publication_date')
    context = {
        'books': books
    }
    return render(request, 'book_temps/book_overview.html', context)

def book_detail(request, pk):
    detail = Book.objects.get(pk= pk)
    context = {
        'detail': detail
    }
    return render(request,'book_temps/book_detail.html', context)

def borrow(request, pk):
    # if Borrow > 2:
    #     messages.success(request, ("You cant borrow more than 2 books at a time"))
    #     return redirect('library:book_detail')

    borrowbook = Book.objects.get(pk= pk)
    form = BorrowForm(request.POST or None)
    if request.method == "POST":
        if borrowbook.status == "Unavailable":
            messages.success(request, ("Oops....book is unavailable, please try again someother time"))
            return  redirect('library:message')
        if form.is_valid():
            # if borrowbook.status == "Unavailable":
            #     messages.success(request, ("Oops....book is unavailable, please try again someother time"))
            form.save()
            messages.success(request, ("You've successfully reserved a book,you are required to get it in the next 24 hours from the library.Check your activity"))
            borrowbook.status = "Unavailable"
            borrowbook.save() 
            return redirect ('library:activity') 
    #instance=borrowbook
    context = {
        'form': form
    }
    return render(request, 'book_temps/borrow.html',context)


def urlmessage(request):
    return render(request, 'book_temps/urlmessage.html',{})
        
    

def activity(request):
    laforms = Borrow.objects.all
    context ={
        'laforms' : laforms
    }
    return render(request, 'book_temps/activity.html',context)


    


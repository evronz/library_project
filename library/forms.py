from django import forms
from django.forms import ModelForm
from .models import Borrow


class BorrowForm(ModelForm):
    class Meta:
        model = Borrow
        fields = ['book_title',
            'email_address',
            ]
        # widgets = {
        #     'book_title': forms.TextInput(attrs={"class": "form-input","placeholder":"Select the book your taking"}),
        #      'email_address': forms.EmailInput(attrs={'class': 'form-input','placeholder':'Enter your email address eg someone@gmail.com'}),
         #}
        labels = {
            'book_title':'',
            'email_address':''
        }

from django.db import models
from django.contrib.auth.models import User
#from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class Student(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    course = models.CharField(max_length=20)
    #id = PhoneNumberField(null=False, blank=False, unique=True)
    

    def __str__(self):
        return str(self.user)
